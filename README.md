
# Start Nacos

1. Download Nacos from https://github.com/alibaba/nacos/releases/

1. ``cd nacos/bin`` and run either ``sh startup.sh -m standalone`` for Linux or ``startup.cmd`` for Windows.

# Config Management

1. After running nacos-boilerplate, run curl against ``http://localhost:8080/config/localcache`` and the output should be ``false``.

1. Run the following against Nacos API to change useLocalCache to true
   ```
   curl -X POST "http://127.0.0.1:8848/nacos/v1/cs/configs?dataId=example&group=DEFAULT_GROUP&content=useLocalCache=true"
   ```
1. Repeat step 1 and the output should become ``true``
1. On Nacos console, the Configuration Management page should look like the following
   ![Nacos Config Mgmt](assets/nacos-config-mgmt.png)
   
# Discovery Management

1. Run curl against ``http://localhost:8080/discovery/instances?serviceName=boilerplate`` and the result should be an empty array.

1. Run the following against Nacos API to register boilerplate service instance
   ```
   curl -X PUT "http://127.0.0.1:8848/nacos/v1/ns/instance?serviceName=boilerplate&ip=127.0.0.1&port=8080"
   ```
1. Re-run step 1 and the results should be
   ```
   $ curl -X GET http://localhost:8080/discovery/instances?serviceName=boilerplate
   [{"instanceId":"127.0.0.1#8080#DEFAULT#boilerplate","ip":"127.0.0.1","port":8080,"weight":1.0,"healthy":true,"cluster":{"serviceName":null,"name":"","healthChecker":{"type":"TCP"},"defaultPort":80,"defaultCheckPort":80,"useIPPort4Check":true,"metadata":{}},"service":null,"metadata":{}}]
   ```
   
1. On Nacos console, the Service List page should look like the following
      ![Nacos Discovery](assets/nacos-discovery.png)