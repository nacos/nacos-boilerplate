package samples.nacos.web.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

@Controller
@RequestMapping("config")
public class ConfigController {

	@Value("${useLocalCache:false}")
	private boolean useLocalCache;

	public void setUseLocalCache(boolean useLocalCache) {
		this.useLocalCache = useLocalCache;
	}

	@RequestMapping(value = "/localcache", method = GET)
	@ResponseBody
	public boolean getUseLocalCache() {
		return useLocalCache;
	}
}